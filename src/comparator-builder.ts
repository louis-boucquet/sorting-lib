import { Comparator } from './comparator';

export function chainComparators<T>(
	comparators: Comparator<T>[],
): Comparator<T> {
	return (a, b) => {
		for (const comparator of comparators) {
			const comparison = comparator(a, b);
			if (comparison !== 0) {
				return comparison;
			}
		}
		return 0;
	};
}

function invertComparator<T>(
	comparator: Comparator<T>,
): Comparator<T> {
	return (a, b) => -comparator(a, b);
}

interface ComparatorBuilder<T> {
	add(otherComparator: Comparator<T>): ComparatorBuilder<T>;

	build(): Comparator<T>;

	invert(): ComparatorBuilder<T>;
}

export function comparatorBuilder<T>(): ComparatorBuilder<T> {
	let comparator: Comparator<T> = () => 0;
	return {
		add(otherComparator: Comparator<T>): ComparatorBuilder<T> {
			comparator = chainComparators([comparator, otherComparator]);

			return this;
		},
		build(): Comparator<T> {
			return comparator;
		},
		invert(): ComparatorBuilder<T> {
			comparator = invertComparator(comparator);

			return this;
		},
	};
}