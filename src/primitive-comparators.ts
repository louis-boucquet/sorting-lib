import { Comparator } from './comparator';

export type Primitive = string | number | Date | boolean | bigint;

export const comparePrimitive: Comparator<Primitive> = (a, b) => {
	return a > b ? 1 : a < b ? -1 : 0;
};
