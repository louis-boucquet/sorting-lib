export type Nil = null | undefined;

export function isNotNil<T>(t: T | Nil): t is T {
	return t !== null && t !== undefined;
}

export function isNil<T>(t: T | Nil): t is Nil {
	return t === null || t === undefined;
}

export function isTruthy<T>(t: T): boolean {
	return !!t;
}

export function isFalsy<T>(t: T): boolean {
	return !t;
}
