import { comparing, NilAs } from './comparing';

describe('comparing', () => {
	it('should compare by primitiveGetter', () => {
		const values = [{ value: 2 }, { value: 1 }];
		expect(values.sort(comparing(item => item.value)))
			.toEqual([{ value: 1 }, { value: 2 }]);
	});

	it('should treat nil as Max', () => {
		const values = [{ value: 2 }, {}];
		expect(values.sort(comparing(item => item.value, NilAs.Max)))
			.toEqual([{ value: 2 }, {}]);
	});

	it('should treat nil as Min', () => {
		const values = [{ value: 2 }, {}];
		expect(values.sort(comparing(item => item.value, NilAs.Min)))
			.toEqual([{}, { value: 2 }]);
	});

	it('should compare by key', () => {
		const values = [
			{ value: 2 },
			{ value: 1 },
		];

		expect(values.sort(comparing('value')))
			.toEqual([{ value: 1 }, { value: 2 }]);
	});

	it('should compare by complex key', () => {
		const values = [
			{ object: { subValue: 2 } },
			{ object: { subValue: 1 } },
		];
		expect(values.sort(comparing('object.subValue')))
			.toEqual([
				{ object: { subValue: 1 } },
				{ object: { subValue: 2 } },
			]);
	});

	it('should compare by complex key and nullable value', () => {
		const values: { object: { subValue?: number } }[] = [
			{ object: { subValue: 2 } },
			{ object: {} },
		];
		expect(values.sort(comparing('object.subValue')))
			.toEqual([
				{ object: { subValue: 1 } },
				{ object: { subValue: 2 } },
			]);
	});
});
