import { chainComparators, comparatorBuilder } from './comparator-builder';
import { comparing } from './comparing';

describe('comparator builder', () => {
	type Type = { id: number, key1: string, key2: number };
	let values: Type[];

	beforeEach(() => {
		values = [
			{ id: 1, key1: 'bcd', key2: 2 },
			{ id: 2, key1: 'bcd', key2: 1 },
			{ id: 3, key1: 'abc', key2: 2 },
			{ id: 4, key1: 'abc', key2: 1 },
		];
	});

	it('should build a comparator', () => {
		const sorted = values
			.sort(
				comparatorBuilder<Type>()
					.add(comparing('key1'))
					.add(comparing('key2'))
					.build(),
			)
			.map(({ id }) => id);

		expect(sorted).toEqual([4, 3, 2, 1]);
	});

	it('should invert the comparator', () => {
		const sorted = values
			.sort(
				comparatorBuilder<Type>()
					.add(comparing(item => item.key1))
					.add(comparing(item => item.key2))
					.invert()
					.build(),
			)
			.map(({ id }) => id);

		expect(sorted).toEqual([1, 2, 3, 4]);
	});

	it('should chain comparators', () => {
		const sorted = values
			.sort(
				chainComparators([
					comparing(item => item.key1),
					comparing(item => item.key2),
				]),
			)
			.map(({ id }) => id);

		expect(sorted).toEqual([4, 3, 2, 1]);
	});
});
