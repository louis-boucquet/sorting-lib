import { isFalsy, isNil, isNotNil, isTruthy } from './nil';

describe('nil.ts', () => {
	function testFunction(
		f: (input: any) => boolean,
		cases: { input: any, output: any }[],
	) {
		for (const { input, output } of cases) {
			it(`should return ${output} for ${JSON.stringify(input)}`, () => {
				expect(f(input)).toBe(output);
			});
		}
	}

	describe('isNotNil', () => {
		testFunction(isNotNil, [
			{ input: null, output: false },
			{ input: undefined, output: false },
			{ input: 0, output: true },
			{ input: 1, output: true },
			{ input: '', output: true },
			{ input: '1', output: true },
			{ input: [], output: true },
			{ input: {}, output: true },
			{ input: false, output: true },
			{ input: true, output: true },
		]);

	});

	describe('isNil', () => {
		testFunction(isNil, [
			{ input: null, output: true },
			{ input: undefined, output: true },
			{ input: 0, output: false },
			{ input: 1, output: false },
			{ input: '', output: false },
			{ input: '1', output: false },
			{ input: [], output: false },
			{ input: {}, output: false },
			{ input: false, output: false },
			{ input: true, output: false },
		]);
	});

	describe('isFalsy', () => {
		testFunction(isFalsy, [
			{ input: null, output: true },
			{ input: undefined, output: true },
			{ input: 0, output: true },
			{ input: 1, output: false },
			{ input: '', output: true },
			{ input: '1', output: false },
			{ input: [], output: false },
			{ input: {}, output: false },
			{ input: false, output: true },
			{ input: true, output: false },
		]);

	});

	describe('isTruthy', () => {
		testFunction(isTruthy, [
			{ input: null, output: false },
			{ input: undefined, output: false },
			{ input: 0, output: false },
			{ input: 1, output: true },
			{ input: '', output: false },
			{ input: '1', output: true },
			{ input: [], output: true },
			{ input: {}, output: true },
			{ input: false, output: false },
			{ input: true, output: true },

		]);
	});
});
