import { comparePrimitive } from './primitive-comparators';

describe('primitive comparators', () => {
	it('should compare numbers', () => {
		expect(comparePrimitive(2, 1))
			.toBeGreaterThan(0);
	});

	it('should compare strings alphabetically', () => {
		expect(comparePrimitive('b', 'a'))
			.toBeGreaterThan(0);
	});

	it('should compare strings locally', () => {
		expect(comparePrimitive('b', 'a'))
			.toBeGreaterThan(0);
	});

	it('should compare dates', () => {
		expect(comparePrimitive(new Date('2021/01/02'), new Date('2021/01/01')))
			.toBeGreaterThan(0);
	});

	it('should compare booleans', () => {
		expect(comparePrimitive(true, false))
			.toBeGreaterThan(0);
	});
});