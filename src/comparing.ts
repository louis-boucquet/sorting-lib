import { comparePrimitive, Primitive } from './primitive-comparators';
import { Comparator } from './comparator';
import { isNotNil, Nil } from './nil';

export interface KeyGetter<T> {
	(t: T): Primitive | Nil;
}

export enum NilAs {
	Min,
	Max,
}

type NestedKey<T, K extends keyof T> = K extends string
	? `${K}.${KeysPath<T[K]>}`
	: never;

type KeysPath<T> = {
	[K in keyof T]-?: K extends string
		? T[K] extends Primitive | Nil
			? K
			: NestedKey<T, K>
		: never
}[keyof T];

export function comparing<T>(
	key: KeysPath<T>,
): Comparator<T>;
export function comparing<T>(
	key: KeysPath<T>,
	remapNilTo: NilAs,
): Comparator<T>;
export function comparing<T>(
	primitiveGetter: KeyGetter<T>,
): Comparator<T>;
export function comparing<T>(
	primitiveGetter: KeyGetter<T>,
	remapNilTo: NilAs,
): Comparator<T>;

export function comparing<T>(
	primitiveGetterOrKey: KeyGetter<T> | KeysPath<T>,
	putNilAt: NilAs = NilAs.Max,
): Comparator<T> {
	if (primitiveGetterOrKey instanceof Function)
		return (a, b) => {
			const primitiveA = primitiveGetterOrKey(a);
			const primitiveB = primitiveGetterOrKey(b);

			if (isNotNil(primitiveA) && isNotNil(primitiveB))
				return comparePrimitive(primitiveA, primitiveB);

			if (isNotNil(primitiveB))
				return putNilAt === NilAs.Max ? 1 : -1;
			if (isNotNil(primitiveA))
				return putNilAt === NilAs.Max ? -1 : 1;

			return 0;
		};

	return comparing(
		item => primitiveGetterOrKey
			.split('.')
			.reduce((total, key) =>
				(total as any)?.[key], item) as unknown as Primitive,
	);
}
